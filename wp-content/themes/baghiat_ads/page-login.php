<?php
/**
 * Login page template
 * 
 * Template Name: Login
 * 
 * @package Baghiat
 */

get_header();

$current_title = get_the_title();
$event_slug =  isset($_GET['event_slug']) ? $_GET['event_slug']: "";
$invite_code = isset($_GET['invite_code']) ? $_GET['invite_code'] : "";


?>

<main class="main overflow-x-hidden min-h-full flex justify-center p-4 ">
    
    <div class="w-full h-fit mt-40 bg-white rounded-3xl border-shadow border-2 border-primary sm:max-w-md xl:p-0 ">
        <div class="p-6 space-y-4 md:space-y-6 sm:p-8 rounded-xl">
        <h1 class="text-primary text-center">ورود به پنل کاربری</h1>
        <form id="phone_form" class="space-y-4 md:space-y-6 flex flex-row gap-4 items-end" method="post" >
            <div class="w-full">
                <label for="" class="block mb-2 text-sm font-medium text-slate-500 ">تلفن همراه</label>
                <div class="w-full flex flex-row gap-2 h-10">
                    <input type="text" name="phone" id="phone" class="h-full bg-gray-50  border border-slate-300 outline-none text-gray-900 sm:text-sm rounded-lg focus:ring-primary focus:border-primary block w-full p-2.5 " 
                    required="">

                    <button id="submit_phone" type="submit" name="submit_phone" class="h-full disabled:bg-cyan-700 text-nowrap text-white bg-primary-gradient focus:outline-none focus:ring-primary-300 rounded-lg text-xs sm:text-sm px-2 sm:px-5 py-2.5 text-center">
                    ارسال کد تایید
                    </button>
                </div>
            </div>
            
            
        </form>
        <!-- <div id="timer" class=""></div> -->

        <form class="space-y-4 md:space-y-6" method="post" action="#" >
            <div>
                <label for="" class="block mb-2 text-sm font-medium text-slate-500 ">کد تایید</label>
                <input  name="code" class="bg-gray-50 border h-10 border-slate-300 outline-none text-gray-900 sm:text-sm rounded-lg focus:ring-primary focus:border-primary block w-full p-2.5 " 
                required="">
            </div>
            <div id="message" class="text-red-500 text-sm">
            </div>
            <button id="submit_final" type="submit" name="submit_final" class="w-full text-white bg-primary-gradient hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center">ورود</button>
        </form>

        <?php if($event_slug){?>
            <a 
                id="go-back-register" 
                href="<?php echo get_site_url() ?>/events/<?php echo $event_slug ?><?php echo $invite_code ?'?invite_code='.$invite_code :'' ?>" 
                class="w-full text-white bg-thirdary-gradient font-medium rounded-lg text-sm px-5 py-2.5 text-center hidden">
                برو به ثبت نام
            </a>
        <?php } ?>
        </div>
    
    </div>


</main>
<?php

get_footer();