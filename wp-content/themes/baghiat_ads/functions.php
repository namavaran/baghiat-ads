<?php
/**
 * Theme Functions.
 *
 * @package Baghiat_Ads
 */


//  here we are defining a variable to store the get_template_directory().
if(! defined('BAGHIAT_ADS_DIR_PATH')) {
    define('BAGHIAT_ADS_DIR_PATH', untrailingslashit(get_template_directory()));
}
//  here we are defining a variable to store the get_template_directory_uri().
if (!defined('BAGHIAT_ADS_DIR_URI')) {
    define('BAGHIAT_ADS_DIR_URI', untrailingslashit(get_template_directory_uri()));
}

if (!defined('BAGHIAT_ADS_BUILD_URI')) {
    define('BAGHIAT_ADS_BUILD_URI', untrailingslashit(get_template_directory_uri()) . '/assets/build');
}

if (!defined('BAGHIAT_ADS_BUILD_JS_URI')) {
    define('BAGHIAT_ADS_BUILD_JS_URI', untrailingslashit(get_template_directory_uri()) . '/assets/build/js');
}

if (!defined('BAGHIAT_ADS_SRC_JS_URI')) {
    define('BAGHIAT_ADS_SRC_JS_URI', untrailingslashit(get_template_directory_uri()) . '/assets/src/js');
}

if (!defined('BAGHIAT_ADS_BUILD_JS_DIR_PATH')) {
    define('BAGHIAT_ADS_BUILD_JS_DIR_PATH', untrailingslashit(get_template_directory()) . '/assets/build/js');
}

if (!defined('BAGHIAT_ADS_IMG_URI')) {
    define('BAGHIAT_ADS_IMG_URI', untrailingslashit(get_template_directory_uri()) . '/assets/src/img');
}
if (!defined('BAGHIAT_ADS_ICON_URI')) {
    define('BAGHIAT_ADS_ICON_URI', untrailingslashit(get_template_directory_uri()) . '/assets/src/icon');
}

if (!defined('BAGHIAT_ADS_BUILD_CSS_URI')) {
    define('BAGHIAT_ADS_BUILD_CSS_URI', untrailingslashit(get_template_directory_uri()) . '/assets/build/css');
}

if (!defined('BAGHIAT_ADS_SRC_CSS_URI')) {
    define('BAGHIAT_ADS_SRC_CSS_URI', untrailingslashit(get_template_directory_uri()) . '/assets/src/css');
}

if (!defined('BAGHIAT_ADS_BUILD_CSS_DIR_PATH')) {
    define('BAGHIAT_ADS_BUILD_CSS_DIR_PATH', untrailingslashit(get_template_directory()) . '/assets/build/css');
}


require_once BAGHIAT_ADS_DIR_PATH . '/inc/helpers/autoloader.php';
require_once BAGHIAT_ADS_DIR_PATH . '/inc/helpers/helper-functions.php';

//  here we are creating a function to create an instance of ADS_THEME (it's a class inside the class-alquran-theme.php) which will load all the other required classes that we create inside the classes directory

function baghiat_ads_get_theme_instance() {
    \ADS_THEME\Inc\ADS_THEME::get_instance();
}

baghiat_ads_get_theme_instance();



// function custom_remove_cpt_slug( $post_link, $post, $leavename ) {
//     if ( 'baghiat_ads_events' != $post->post_type || 'publish' != $post->post_status ) {
//         return $post_link;
//     }
//     $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
//     return $post_link;
// }
// add_filter( 'post_type_link', 'custom_remove_cpt_slug', 10, 3 );

// function custom_parse_request( $query ) {
//     if ( ! $query->is_main_query() ) {
//         return;
//     }
//     if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
//         return;
//     }
//     if ( ! empty( $query->query['name'] ) ) {
//         $query->set( 'post_type', array( 'post', 'baghiat_ads_events', 'page' ) );
//     }
// }
// add_action( 'pre_get_posts', 'custom_parse_request' );

// function custom_remove_cpt_slug( $post_link, $post, $leavename ) {
//     if ( 'baghiat_ads_events' != $post->post_type || 'publish' != $post->post_status ) {
//         return $post_link;
//     }
//     $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
//     return $post_link;
// }
// add_filter( 'post_type_link', 'custom_remove_cpt_slug', 10, 3 );

// function custom_parse_request( $query ) {
//     if ( ! $query->is_main_query() ) {
//         return;
//     }
//     if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
//         return;
//     }
//     if ( ! empty( $query->query['name'] ) ) {
//         $query->set( 'post_type', array( 'post', 'baghiat_ads_events', 'page' ) );
//     }
// }
// add_action( 'pre_get_posts', 'custom_parse_request' );


function create_database()
{
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $charset_collate = $wpdb->get_charset_collate();

    // Creating user table
    $sql_data = "CREATE TABLE {$wpdb->prefix}aw_users (
        id INT PRIMARY KEY AUTO_INCREMENT,
        user_id INT NOT NULL,
        event_id INT NOT NULL,
        full_name VARCHAR(255),
        phone_number VARCHAR(13),
        invite_link TEXT,
        invited_by INT,
        register_date DATETIME
    ) $charset_collate;";
    dbDelta($sql_data);
}
// create_database();