<?php
/**
 * Header template.
 *
 * @package Baghiat_Ads
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="description" content="this is baghiat website">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?> 
</head>
<body <?php body_class(); ?>>
    <?php 
    if (function_exists('wp_body_open')) {
        wp_body_open();
    }    
    ?> 

<div class="fixed -z-50 w-screen h-screen">
<div class="absolute ba1 top-4 -left-40 w-[800px] rounded-full aspect-square bg-orange-300 blur-3xl -z-50"></div>
<div class="absolute ba2 top-36 -right-40 w-[500px] rounded-full aspect-square bg-sky-300 blur-[100px] -z-500"></div>
</div>

<div class="w-full flex justify-center items-center">
    <header class="container w-full mx-auto py-5">
        <!-- adding nav -->
        <?php get_template_part('template-parts/header/nav') ?>
    </header>
</div>