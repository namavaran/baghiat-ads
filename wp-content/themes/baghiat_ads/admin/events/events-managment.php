<?php

global $wpdb;
$users_per_page = 10;
$paged = isset($_GET['paged']) ? absint($_GET['paged']) : 1;

$args = array(
    'post_type'      => 'events',
    'posts_per_page' => $users_per_page,
    'paged'          => $paged,
    'orderby'        => 'ID',
    'order'          => 'ASC',
    'post_status'    => 'publish'
);

$query = new WP_Query($args);

$events = [];

if ($query->have_posts()) {
    while ($query->have_posts()) { 
        $query->the_post();
        $events[] = [
            'id'      => get_the_ID(),
            'title'   => get_the_title(),
            'content' => get_the_content(),
            'end_date' => get_post_meta(get_the_ID(),'baghiat_ads_event_start_date',true),
            'start_date' => get_post_meta(get_the_ID(),'baghiat_ads_event_finish_date',true)
        ];
    }
    
} else {
    echo 'No events found';
}

wp_reset_postdata();

// echo "<pre>";
// var_dump($events);
// echo "</pre>";



?>



<div class="wrap">
    <h1 class="wp-heading-inline">اطلاعات کاربران ثبت نام کرده در طرح</h1>
    <hr class="wp-header-end">



    <div id="users_table"></div>
    <table class="wp-list-table widefat fixed striped text-center">
        <thead>
            <tr>
                <th scope="col" class="manage-column">شناسه</th>
                <th scope="col" class="manage-column">نام</th>
                <th scope="col" class="manage-column">تاریخ شروع</th>
                <th scope="col" class="manage-column">تاریخ پایان</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (empty($events)) {
            ?>

                <tr>
                    <td class="">موردی یافت نشد</td>
                </tr>
            <?php
            }
            foreach ($events as $event) : ?>
                <?php
                $event_id = $event['id'];
                $event_title = $event['title'];
                $event_start_date = $event['start_date'];
                $post_end_date = $event['end_date'];
                ?>
                <tr>
                    <td>
                        <a href="<?php echo admin_url('admin.php?page=baghiat-ads-user-details&event_id=').$event_id ?>"><?php echo $event_id; ?></a>
                        
                    </td>
                    <td>
                        <?php echo $event_title; ?>
                    </td>
                    <td>
                        <?php echo $event_start_date; ?>
                    </td>
                    <td>
                        <?php echo $post_end_date; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


    <?php


    $total_pages = $query->max_num_pages;
        
    // Display pagination
    $big = 999999999; // need an unlikely integer

    $pagination = paginate_links(array(
        'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format'    => '?paged=%#%',
        'current'   => max(1, $paged),
        'total'     => $total_pages,
        'prev_text' => __('« Prev'),
        'next_text' => __('Next »'),
    ));

    if ($pagination) {
        echo '<div class="pagination">' . $pagination . '</div>';
    }

    ?>
</div>