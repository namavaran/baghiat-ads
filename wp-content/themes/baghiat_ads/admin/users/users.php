<?php


$event_id = isset($_GET['event_id']) ? absint($_GET['event_id']) : 1;

$event_title = get_the_title($event_id);


global $wpdb;
$tablename = $wpdb->prefix . 'aw_users';
$users = $wpdb->get_results("SELECT * FROM $tablename WHERE event_id = $event_id");


// var_dump( $users );

?>

<div class="wrap">
    <h1 class="wp-heading-inline">اطلاعات کاربران ثبت نام کرده در رویداد 
        <strong>
            <?php echo $event_title ?>
        </strong>
    </h1>
    <hr class="wp-header-end">



    <div id="users_table"></div>
    <table class="wp-list-table widefat fixed striped text-center">
        <thead>
            <tr>
                <th scope="col" class="manage-column">شناسه</th>
                <th scope="col" class="manage-column">نام</th>
                <th scope="col" class="manage-column">شماره تماس</th>
                <th scope="col" class="manage-column">دعوت شده توسط</th>
                <th scope="col" class="manage-column">تاریخ ثبت نام</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (empty($users)) {
            ?>

                <tr>
                    <td class="">موردی یافت نشد</td>
                </tr>
            <?php
            }
            foreach ($users as $user) : ?>
                <?php
                $user_id = $user->user_id;
                $user_name = $user->full_name;
                $user_phone = $user->phone_number;
                $user_invited_by = $user->invited_by;
                $user_register_date = $user->register_date;
                ?>
                <tr>
                    <td>
                        <!-- <a href="<?php echo admin_url('admin.php?page=baghiat-ads-user-details&event_id=').$event_id ?>"><?php echo $event_id; ?></a> -->
                        <?php echo $user_id; ?>
                    </td>
                    <td>
                        <?php echo $user_name; ?>
                    </td>
                    <td>
                        <?php echo $user_phone; ?>
                    </td>
                    <td>
                        <?php echo $user_invited_by; ?>
                    </td>
                    <td>
                        <?php echo $user_register_date; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


    <!-- <?php


    $total_pages = $query->max_num_pages;
        
    // Display pagination
    $big = 999999999; // need an unlikely integer

    $pagination = paginate_links(array(
        'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format'    => '?paged=%#%',
        'current'   => max(1, $paged),
        'total'     => $total_pages,
        'prev_text' => __('« Prev'),
        'next_text' => __('Next »'),
    ));

    if ($pagination) {
        echo '<div class="pagination">' . $pagination . '</div>';
    }

    ?> -->
</div>
