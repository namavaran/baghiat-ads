<?php
/**
 * post types class
 *
 * @package Baghiat_Ads
 */

namespace ADS_THEME\Inc;

use ADS_THEME\Inc\Traits\Singleton;

class Custom_Post_Types
{
    use Singleton;

    protected function __construct()
    {
        // Load classes.
        $this->setup_hooks();
    }

    protected function setup_hooks()
    {
        /**
         * Actions.
         */
        add_action('init', [$this, 'baghiat_ads_events_post_type'], 0);
    }

    // Register Custom Post Type
    function baghiat_ads_events_post_type()
    {

        $labels = array(
            'name' => _x('رویداد ها', 'Post Type General Name', 'baghiat_ads'),
            'singular_name' => _x('رویداد', 'Post Type Singular Name', 'baghiat_ads'),
            'menu_name' => __('رویداد ها', 'baghiat_ads'),
            'name_admin_bar' => __('رویداد ها', 'baghiat_ads'),
            'archives' => __('آرشیو', 'baghiat_ads'),
            'attributes' => __('ویژگی های رویداد', 'baghiat_ads'),
            'parent_item_colon' => __('پدر', 'baghiat_ads'),
            'all_items' => __('همه ی رویداد ها', 'baghiat_ads'),
            'add_new_item' => __('یک رویداد جدید ایجاد کنید', 'baghiat_ads'),
            'add_new' => __('رویداد جدید', 'baghiat_ads'),
            'new_item' => __('رویداد جدید', 'baghiat_ads'),
            'edit_item' => __('ویرایش رویداد', 'baghiat_ads'),
            'update_item' => __('به روزرسانی رویداد', 'baghiat_ads'),
            'view_item' => __('مشاهدۀ رویداد', 'baghiat_ads'),
            'view_items' => __('مشاهدۀ رویداد ها', 'baghiat_ads'),
            'search_items' => __('جستجو در رویداد ها', 'baghiat_ads'),
            'not_found' => __('موردی یافت نشد', 'baghiat_ads'),
            'not_found_in_trash' => __('موردی در سطل زباله یافت نشد', 'baghiat_ads'),
            'featured_image' => __('تصویر رویداد', 'baghiat_ads'),
            'set_featured_image' => __('بارگذاری تصویر', 'baghiat_ads'),
            'remove_featured_image' => __('حذف تصویر', 'baghiat_ads'),
            'use_featured_image' => __('استفاده از تصویر', 'baghiat_ads'),
            'insert_into_item' => __('اضافه کردن به رویداد', 'baghiat_ads'),
            'uploaded_to_this_item' => __('بارگزاری در رویداد', 'baghiat_ads'),
            'items_list' => __('لیست رویداد ها', 'baghiat_ads'),
            'items_list_navigation' => __('ناوبری لیست رویداد ها', 'baghiat_ads'),
            'filter_items_list' => __('فیلتر کردن رویداد ها', 'baghiat_ads'),
        );
        $args = array(
            'label' => __('رویداد', 'baghiat_ads'),
            'description' => __('پست تایپ برای رویداد های سایت', 'baghiat_ads'),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'custom-fields', 'thumbnail'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-palmtree',
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        );
        register_post_type('Events', $args);

    }
}


