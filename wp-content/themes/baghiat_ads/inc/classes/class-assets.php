<?php
/**
 * Enqueue Theme assets
 *
 * @package Baghiat_Ads
 */

//  we choose a namespace name based on the main folder that the file is it in
namespace ADS_THEME\Inc;

use ADS_THEME\Inc\Traits\Singleton;

class Assets {
    use Singleton;

    protected function __construct() {
        //  load classes.
        $this->setup_hooks();
    }

    protected function setup_hooks() {
        /**
         * Actions.
         */
        add_action('wp_enqueue_scripts', [ $this, 'register_styles']);
        add_action('wp_enqueue_scripts', [ $this, 'register_scripts']);

    }

    public function register_styles() {
        // Register Styles
        wp_register_style('style-css', get_stylesheet_uri(), [], filemtime( BAGHIAT_ADS_DIR_PATH . '/style.css'), 'all' );
        wp_register_style('output-css', BAGHIAT_ADS_BUILD_CSS_URI . '/output.css', [], filemtime(BAGHIAT_ADS_BUILD_CSS_DIR_PATH . '/output.css'), 'all');

        // Enqueue Styles
        wp_enqueue_style('style-css');
        wp_enqueue_style('output-css');
    }

    public function register_scripts() {
        // $page_slug = get_post_field( 'post_name', get_post() );
        // Register Scripts
        // wp_register_script('main-js', BAGHIAT_ADS_BUILD_JS_URI . '/main.js', ['jquery'], filemtime(BAGHIAT_ADS_BUILD_JS_DIR_PATH . '/main.js'), true);
        wp_register_script('main-js', BAGHIAT_ADS_SRC_JS_URI . '/main.js', ['jquery'], filemtime(BAGHIAT_ADS_BUILD_JS_DIR_PATH . '/main.js'), true);
        wp_register_script('ajax-registration', BAGHIAT_ADS_SRC_JS_URI . '/ajax-registeration.js', array('jquery'), null, true);
        wp_register_script('ajax-login', BAGHIAT_ADS_SRC_JS_URI . '/ajax-login.js', array('jquery'), null, true);
        wp_register_script('single-page', BAGHIAT_ADS_SRC_JS_URI . '/single-page.js', array('jquery'), null, true);
        wp_localize_script('ajax-registration', 'ajax_object', array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'ajax_nonce' => wp_create_nonce('ajax-register-nonce')
        ));
        wp_localize_script('ajax-login', 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));


        // Enqueue Scripts
        wp_enqueue_script('main-js');
        wp_enqueue_script('ajax-registration');
        wp_enqueue_script('ajax-login');
        wp_enqueue_script('single-page');


    }
}