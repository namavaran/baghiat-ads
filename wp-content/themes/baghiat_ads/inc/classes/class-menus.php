<?php
/**
 * Register Menus
 * 
 * @package Baghiat
 */

//  we choose a namespace name based on the main folder that the file is it in
namespace ADS_THEME\Inc;

use ADS_THEME\Inc\Traits\Singleton;

class Menus {
    use Singleton;

    protected function __construct() {
        //  load classes.
        $this->setup_hooks();
    }

    protected function setup_hooks() {
        /**
         * Actions.
         */
        add_action( 'init', [ $this, 'register_menus' ] );
    }

    public function register_menus() {
        register_nav_menus([
            'baghiat-ads-header-menu' => esc_html__( 'Header Menu', 'baghiat' ),
            'baghiat-ads-footer-menu' => esc_html__( 'Footer Menu', 'baghiat' ), 
            // 'baghiat-about-menu' => esc_html__( 'About Menu', 'baghiat' ),
            'baghiat-ads-panel-menu' => esc_html__( 'Panel Menu', 'baghiat' )
        ]);
    }

    // this function gets the menu location and then returns the menu_id.
    public function get_menu_id( $location ) {
        // Get all the locations.
        $locations = get_nav_menu_locations();

        // Get ofject id by location.
        $menu_id = $locations[$location];

        return ! empty( $menu_id ) ? $menu_id : '';
    }

    // this function gets the menu array, parent id and then returns the all child menu items in an array.
    public function get_child_menu_items( $menu_array, $parent_id ) {

        $child_menus = [];

        if ( ! empty( $menu_array ) && is_array ( $menu_array) ) {
            foreach ( $menu_array as $menu ) {
                if ( intval( $menu -> menu_item_parent ) === $parent_id ) {
                    array_push ( $child_menus, $menu);
                }
            }
        }

        return $child_menus;
    }

}