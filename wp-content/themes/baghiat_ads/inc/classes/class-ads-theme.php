<?php
/**
 * Bootstraps the Theme.
 *
 * @package Baghiat_Ads
 */

namespace ADS_THEME\Inc;

use ADS_THEME\Inc\Traits\Singleton;
use SoapClient;
class ADS_THEME
{
    use Singleton;

    protected function __construct()
    {

        //  load all the classes that we create inside the classes directory.

        Assets::get_instance();
        Menus::get_instance();
        Custom_Post_Types::get_instance();
        Admin_Pages::get_instance();

        $this->setup_hooks();
    }

    protected function setup_hooks()
    {
        /**
         * Actions.
         */

        add_action('after_setup_theme', [$this, 'setup_theme']);
        add_action('wp_ajax_nopriv_register_user', [$this, 'handle_ajax_registration']);
        add_action('wp_ajax_register_user', [$this, 'handle_ajax_registration']);
        add_action('wp_ajax_nopriv_register_user_phone', [$this, 'handle_ajax_register_user_phone']);
        add_action('wp_ajax_register_user_phone', [$this, 'handle_ajax_register_user_phone']);
        add_action('wp_ajax_nopriv_login_user', [$this, 'handle_ajax_login']);
        add_action('wp_ajax_login_user', [$this, 'handle_ajax_login']);
        add_action('wp_ajax_nopriv_login_user_finel', [$this, 'handle_ajax_login_finel']);
        add_action('wp_ajax_login_user_finel', [$this, 'handle_ajax_login_finel']);
        add_action('after_setup_theme', [$this, 'remove_admin_bar']);


        /**
         * Filters.
         */
        // add_filter( 'upload_mimes', [ $this, 'allow_svg_upload' ] );

    }

    public function setup_theme()
    {
        add_theme_support('title-tag');
        add_theme_support('custom-logo', [
            'header-text' => ['site-title', 'site-description'],
            'height' => 100,
            'width' => 400,
            'flex-height' => true, // lets us to change logo's height
            'flex-width' => true, // lets us to change logo's width
        ]);
        add_theme_support('custom-background', [
            'default-color' => '#fff',
            'default-image' => '',
            'default-repeat' => 'repeat',
        ]);

        add_theme_support('post-thumbnails');

        //  Register image sizes.
        // add_image_size( 'featured-thumbnail', 385, 251, true );

        add_theme_support('customize-selective-refresh-widgets');
        add_theme_support('automatic-feed-links');
        add_theme_support('html5', [
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
            'script',
            'style',
        ]);
        add_theme_support('wp-block-styles');
        add_theme_support('align-wide');
        add_theme_support('editor-styles'); // this will allow us to use our custom styles for the backend editor 
        add_editor_style('styles.css'); // we give it the css file which has the editor's styles

        //  Remove the core block patterns 
        // remove_theme_support('core-block-patterns');

        global $content_width;
        if (!isset($content_width)) {
            $content_width = 1240;
        }
    }
    public function allow_svg_upload()
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    public function handle_ajax_registration() {
        // Check for nonce security
        if (!isset($_POST['security']) || !wp_verify_nonce($_POST['security'], 'ajax-register-nonce')) {
            wp_send_json_error('Invalid nonce');
            return;
        }
    
        // Extract name and phone number from the form
        $full_name = sanitize_text_field($_POST['full_name']);
        $phone_number = sanitize_text_field($_POST['phone_number']);
        $event_id = $_POST['event_id'];
        $invite_code = $_POST['invite_code'];
        $current_date = date("Y-m-d H:i:s");
    
        // Check if the user already exists
        if (username_exists($phone_number) || email_exists($phone_number)) {
            $current_user = get_user_by('login', $phone_number);
            $current_user_id = $current_user->ID;
    
            // Generate a combined code
            $combined_code = $current_user_id . $phone_number . $event_id . $full_name . $current_date;
            // Hash the combined code
            $hashed_code = hash('sha256', $combined_code);
    
            global $wpdb;
            $tablename = $wpdb->prefix . 'aw_users';
            $result = $wpdb->get_results("SELECT * FROM $tablename WHERE user_id = $current_user_id AND event_id = $event_id");
            if ($result) {
                wp_send_json_error('شما قبلاً با این شماره در قرعه کشی ثبت نام کرده اید!');
            } else {
                $inviter = $wpdb->get_results("SELECT * FROM $tablename WHERE invite_link = '$invite_code' AND event_id = $event_id");
    
                $wpdb->insert($tablename, array(
                    'user_id' => $current_user_id,
                    'event_id' => $event_id,
                    'full_name' => $full_name,
                    'phone_number' => $phone_number,
                    'invite_link' => $hashed_code,
                    'invited_by' => $inviter[0]->user_id,
                    'register_date' => $current_date
                ), array('%s', '%s', '%s', '%s', '%s', '%s', '%s'));
    
                // Log the user in
                wp_set_auth_cookie($current_user_id, true);
    
                $redirect_url = add_query_arg(array(
                    'invite_code' => $hashed_code,
                    'event_id' => $event_id,
                ), 'my-account');
                wp_send_json_success(array('redirect' => $redirect_url));
            }
        } else {
            // Generate a random password
            $random_password = wp_generate_password(12);
            // Create user with phone number as username
            $user_id = wp_create_user($phone_number, $random_password);
    
            // Generate a combined code
            $combined_code = $user_id . $phone_number . $event_id . $full_name . $current_date;
            // Hash the combined code
            $hashed_code = hash('sha256', $combined_code);
    
            if (!is_wp_error($user_id)) {
                // User created successfully, update user's display name
                wp_update_user(array('ID' => $user_id, 'display_name' => $full_name));
                global $wpdb;
                $tablename = $wpdb->prefix . 'aw_users';
                $inviter = $wpdb->get_results("SELECT * FROM $tablename WHERE invite_link = '$invite_code' AND event_id = $event_id");
    
                $wpdb->insert($tablename, array(
                    'user_id' => $user_id,
                    'event_id' => $event_id,
                    'full_name' => $full_name,
                    'phone_number' => $phone_number,
                    'invite_link' => $hashed_code,
                    'invited_by' => $inviter[0]->user_id,
                    'register_date' => $current_date
                ), array('%s', '%s', '%s', '%s', '%s', '%s', '%s'));
    
                // Log the user in
                wp_set_auth_cookie($user_id, true);
    
                $redirect_url = add_query_arg(array(
                    'invite_code' => $hashed_code,
                    'event_id' => $event_id,
                ), 'my-account');
                wp_send_json_success(array('redirect' => $redirect_url));
            } else {
                wp_send_json_error('شماره ی موبایل الزامی می باشد!');
            }
        }
    }

    public function handle_ajax_register_user_phone(){
        
        $phone_number = sanitize_text_field($_POST['phone_number']);
        $num = mt_rand(100000,999999);
$text = "کد تایید شما
{$num}
10-k.ir
";
        $result = $this->send_sms_to_users($phone_number,$text);
        wp_send_json_success(array('result' => $result, 'code' => $num ));

    }
    public function handle_ajax_login() {
        
        $phone_number = sanitize_text_field($_POST['phone_number']);
        $user = get_user_by('login', $phone_number);
        $current_user_id = $user->ID;
        $num = mt_rand(100000,999999);
$text = "کد تایید شما
{$num}
10-k.ir
";
        $result = $this->send_sms_to_users($phone_number,$text);
    
        wp_send_json_success(array('result' => $result ,'phone_number' => $phone_number , 'code' => $num , 'user_id' => $current_user_id));
        // echo  $phone_number ;
    }
    
    public function handle_ajax_login_finel(){
        $current_user_id = sanitize_text_field($_POST['user_id']);

        wp_set_auth_cookie($current_user_id, true);
        wp_send_json_success(array('user_id' => $current_user_id));

    }

    protected function send_sms_to_users($number, $text)
    {
        $api = new SoapClient('http://www.tsms.ir/soapWSDL/?wsdl');
        $username = 'nejat';
        $password = 'Vahid1367';
        $messagid = rand();
        $mclass = array('');
        $msg = $text;
        $mobiles = $number;
        $sms_number = '3000101172';
        $result = $api->sendSmsGroup($username, $password, $sms_number, $mobiles, $msg, $mclass, $messagid);
        return $result;
    }

    public function remove_admin_bar() {
        if (!current_user_can('administrator') && !is_admin()) {
          show_admin_bar(false);
        }
    }

}