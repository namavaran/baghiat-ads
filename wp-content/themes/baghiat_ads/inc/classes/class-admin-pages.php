<?php
/**
 * Admin Panel Page
 *
 * @package Baghiat_Ads
 */

//  we choose a namespace name based on the main folder that the file is it in
namespace ADS_THEME\Inc;

use ADS_THEME\Inc\Traits\Singleton;

class Admin_Pages
{
    use Singleton;

    protected function __construct()
    {
        // Load classes.
        $this->setup_hooks();
    }

    protected function setup_hooks()
    {
        /**
         * Actions.
         */
        add_action('admin_menu', [$this, 'register_user_management_pages']);
    }

    public function register_user_management_pages()
    {
        add_menu_page(
            'Events Management',
            'مدیریت رویدادها',
            'manage_options',
            'baghiat-ads-events-management',
            [$this, 'all_users'],
            'dashicons-calendar-alt',
            7
        );
        add_submenu_page(
            null,
            'Users',
            'لیست کاربران',
            'manage_options',
            'baghiat-ads-user-details',
            [$this, 'user_management_page2']
        );
    }

    public function all_users()
    {
        get_template_part('admin/events/events-managment');
    }

    public function user_management_page2()
    {
        get_template_part('admin/users/users');
    }
}


