<?php
/**
 * event registration page
 *
 * @package Baghiat_Ads
 */

get_header();

if (isset($_GET['event_id'])) {
    $event_id = $_GET['event_id'];
    $token = '08sfdMewhPUWKKD3mxUso5fKqREQDgW9TecskH0pYOt2iQzDeOdChTTfrZXmuk8K';
    $hash = hash('sha256', $event_id.'-'.$token);
    $incommingHash = $_GET['hash'];

    if($incommingHash == $hash) {
        ?>
<section class="container mx-auto">
    <div class="Main overflow-hidden">
        <form id="registration-form">
            <section id="section1" class="bg-white border-2 border-primary rounded-3xl">
                <?php get_template_part('template-parts/registeration/title', null, ['title' => 'ثبت اطلاعات']) ?>
                <!-- personal-info -->
                <div>


                    <div class="grid md:grid-cols-2 gap-x-8 gap-y-2 font-vazitmatn px-8 sm:px-14 lg:px-40">
                        <div class="">
                            <span class="block mr-2 mb-[0.625rem] cursor-default">
                                نام و نام خانوادگی
                            </span>
                            <input name="full_name" type="text" placeholder="علی محمدی"
                                class="personal-input border-2 border-primary-200 rounded-xl px-4 py-3 w-full shadow-sm focus:outline-none focus:border-primary" />
                        </div>

                        <div class="">
                            <span class="block mr-2 mb-[0.625rem] cursor-default">شماره موبایل
                            </span>
                            <input id="phone_number" name="phone_number" type="tel" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" minlength="11"
                                placeholder="09302239247"
                                class="personal-input border-2 border-primary-200 mb-2 rounded-xl px-4 py-3 w-full shadow-sm focus:outline-none focus:border-primary" />
                            <span class="error-message text-red-500 text-sm px-3 h-5 block"></span>
                        </div>

                        <div class="">
                            <span class="block mr-2 mb-[0.625rem] cursor-default">کد تایید ارسال شده
                            </span>
                            <div class="flex items-center px-1 border-2 border-primary-200 mb-2 rounded-xl  w-full overflow-hidden shadow-sm focus-within:border-primary">
                                <input name="code" id="code" type="text"
                                placeholder="_ _ _ _ _ _"
                                class="personal-input focus:outline-none inline-block w-full px-4 py-3" />
                                <button id="submit_phone_register" type="button" class="bg-primary-gradient rounded-lg text-nowrap px-4 text-sm py-3 text-white h-full">ارسال کد تایید</button>
                            </div>
                                <span id="message" class="error-message text-red-500 text-sm px-3 h-5 block"></span>
                        </div>
                    </div>
                </div>

                <span class="block w-full h-[1px] bg-slate-200 my-8"></span>

                <div class="flex flex-row justify-center px-8 sm:px-14 lg:px-40 mb-8">
                    <!-- submit button -->
                    <button type="button" id="submit-btn"
                        class="px-6 py-[0.875rem] w-full bg-thirdary-gradient rounded-xl text-white font-vazitmatn font-bold h-fit">
                        ثبت اطلاعات
                    </button>
                </div>
            </section>
            <?php wp_nonce_field('ajax-register-nonce', 'security'); ?>
        </form>
    </div>
</section>
<?php
    }
}

?>



<?php
get_footer();
?>