<?php
$is_login = is_user_logged_in();
if ($is_login === true) {
    $user = wp_get_current_user();
    $display_name = $user->display_name;
    ?>
    <a href="<?php echo site_url() ?>/my-account">
        <button class="px-3 leading-0 py-2 bg-secondary-gradient rounded-lg flex gap-1.5 items-center justify-center">
            <span class="md:hidden xl:block text-white text-sm"><?php echo esc_html($display_name); ?></span>
            <img src="<?php echo esc_url(BAGHIAT_ADS_ICON_URI); ?>/login.svg" alt="login icon" />
        </button>
    </a>
    <?php
} else {
    ?>
    <a href="<?php echo site_url() ?>/login">
        <button class="px-3 leading-0 py-2 bg-secondary-gradient rounded-lg flex gap-1.5 items-center">
            <span class="text-white text-sm font-light"><?php echo esc_html('ورود'); ?></span>
            <img src="<?php echo esc_url(BAGHIAT_ADS_ICON_URI); ?>/login.svg" alt="login icon" />
        </button>
    </a>
    <?php
}
?>