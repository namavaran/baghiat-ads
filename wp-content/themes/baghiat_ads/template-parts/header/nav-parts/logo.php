<?php 
    $order_class = isset($args['order_class']) ? $args['order_class'] : '';

    if ( get_custom_logo() != "" ) {
        ?>
        <span class="<?php echo esc_attr($order_class); ?>">
        <?php
            the_custom_logo();
        ?>
        </span>
        <?php
    } else {
        ?>
        <a href="<?php echo home_url(); ?>" class="<?php echo esc_attr($order_class); ?>">
            <img src="<?php echo BAGHIAT_ADS_IMG_URI ?>/Logo.png" alt="logo img" />
        </a>
        <?php
    }
?>
