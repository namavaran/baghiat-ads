<?php

$menu_class = \ADS_THEME\Inc\Menus::get_instance();
$header_menu_id = $menu_class->get_menu_id('baghiat-ads-header-menu');
$header_menus = wp_get_nav_menu_items($header_menu_id);




if (!empty($header_menus) && is_array($header_menus)) {
    ?>
    <ul id="menuList"
        class="hidden flex-1 basis-full md:basis-auto md:flex-none order-4 md:order-3 bg-white md:bg-transparent md:flex items-center *:cursor-pointer *:py-2 text-text-grey md:text-sm md:leading-18px xl:leading-5">
        <?php
        foreach ($header_menus as $menu_item) {
            if (!$menu_item->menu_item_parent) {
                // Check if the current menu item corresponds to the current page
                $is_current = false;
                if ((is_page() || is_home()) && $menu_item->object_id == get_queried_object_id()) {
                    $is_current = true;
                }
                ?>
                <li class="flex items-center w-full md-w-fit ml-4 gap-4 <?php echo $is_current ? 'active' : ''; ?>">
                    <a href="<?php echo esc_url($menu_item->url); ?>"
                        class="w-full text-nowrap md:px-3 text-sm sm: text:base py-2 rounded-lg text-gray hover:text-navText hover:bg-gradient-0 <?php echo $is_current ? 'active' : ''; ?>">
                        <?php echo $menu_item->title ?>
                    </a>
                </li>
                <?php
            }
        }
        ?>
    </ul>
    <?php
}
?>