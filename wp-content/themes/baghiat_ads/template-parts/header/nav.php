<?php

/**
 * Header Navigation template.
 * 
 * @package Baghiat_Ads
 */

?>




    <nav
        class="innerDiv w-full flex flex-wrap md:flex-nowrap items-center justify-between bg-white border-primary border-shadow rounded-2xl py-2.5 px-4 border-2 md:py-1 xl:py-3 md:px-7 xl:px-8">
        <!-- adding logo -->
        <?php get_template_part('template-parts/header/nav-parts/logo', null, ['order_class' => 'order-2 w-[60px]']) ?>

        <!-- hamburger button in mobile size -->
        <button class="md:hidden bg-primary-100 rounded-lg p-1" id="mobile-menu-opener">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-menu-2" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#0ea5e9" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M4 6l16 0" />
                <path d="M4 12l16 0" />
                <path d="M4 18l16 0" />
            </svg>
        </button>

        <!-- adding menu items -->
        <!-- <?php get_template_part('template-parts/header/nav-parts/menu') ?> -->

        <!-- language selector visibile for tabllet and desktop -->
        <div class="hidden md:flex md:gap-1 xl:gap-4 order-3">
            <!-- login button -->
            <?php get_template_part('template-parts/header/nav-parts/login-button') ?>
        </div>

        <!-- user access in mobile size -->
        <?php
        $is_login = is_user_logged_in();
        if ($is_login === true) {
            $user = wp_get_current_user();
            ?>
            <button class="md:hidden order-2 px-3 leading-0 py-2 bg-primary-100 rounded-lg flex gap-1.5 items-center">
                <a href="<?php echo site_url() ?>/my-account">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#0ea5e9" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                    <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0" />
                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
                </svg>
                </a>
            </button>
            <?php
        } else {
            ?>
            <button class="md:hidden order-2 bg-primary-100 rounded-lg p-1">
            <a href="<?php echo site_url() ?>/my-account">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-user" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#0ea5e9" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0" />
                <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" />
            </svg>
            </a>
            </button>
            <?php
        }
        ?>
    </nav>
