<?php
$title = isset($args['title']) ? $args['title'] : '';
?>
<div class="flex justify-center items-center py-8 px-8 sm:px-14 lg:px-40">
    <span class="min-w-[10px] h-[10px] rounded-full bg-border"></span>
    <span class="w-full border-t-2 border-border"></span>

    <h2 class="font-vazitmatn font-medium  md:text-xl text-active mx-4 min-w-fit cursor-default">
        <?php echo esc_html($title); ?>
    </h2>

    <span class="w-full border-t-2 border-border"></span>
    <span class="min-w-[10px] h-[10px] rounded-full bg-border"></span>
</div>