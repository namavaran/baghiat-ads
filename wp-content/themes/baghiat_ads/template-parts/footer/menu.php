<?php
$menu_class = \ADS_THEME\Inc\Menus::get_instance();
$footer_menu_id = $menu_class->get_menu_id('baghiat-ads-footer-menu');
$footer_menus = wp_get_nav_menu_items($footer_menu_id);


if (!empty($footer_menus) && is_array($footer_menus)) {
    ?>
    <ul class="flex flex-col gap-5 text-gray-800">
        <?php
        foreach ($footer_menus as $menu_item) {
            if (!$menu_item->menu_item_parent) {
                // $child_menu_items = $menu_class->get_child_menu_items($header_menus, $menu_item->ID);
                $has_children = !empty($child_menu_items) && is_array($child_menu_items);
                if (!$has_children) {
                    ?>
                    <li class="text-sm">
                        <a href="<?php echo esc_url($menu_item->url); ?>" class="">
                            <?php echo $menu_item->title ?>
                        </a>
                    </li>
                    <?php
                }
            }
        }
        ?>
    </ul>
    <?php
}

?>