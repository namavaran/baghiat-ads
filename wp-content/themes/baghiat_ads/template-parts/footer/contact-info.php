<?php
// $telegram = get_theme_mod('baghiat_contact_setting_telegram', esc_html__('@safineh_m', 'baghiat'));
// $email = get_theme_mod('baghiat_contact_setting_email', esc_html__('baghiat.alsalehat@gmail.com', 'baghiat'));
// $phone = get_theme_mod('baghiat_contact_setting_phone', esc_html__('+98 912 851 1854', 'baghiat'));
$telegram = "test";
$email = "test@gmail.com";
$phone = "99999999";
?>

<div class="col-span-2 row-span-1 xl:col-span-4 flex flex-col items-center xl:items-start gap-5">
    <h2 class="text-gray-800 text-base"><?php echo 'ارتباط با ما'; ?></h2>
    <div class="w-full flex justify-between">
        <ul class="flex flex-col gap-5 text-gray-800">
            <li>
                <a href="" class=" text-sm"><?php echo 'ایمیل'; ?></a>
            </li>
            <li>
                <a href="" class=" text-sm"><?php echo 'تلگرام | ایتا | بله | روبیکا'; ?></a>
            </li>
            <li>
                <a href="" class=" text-sm"><?php echo 'شماره تماس'; ?></a>
            </li>
        </ul>
        <ul class="flex flex-col gap-5 items-end text-gray-800">
            <li>
                <a href="" class=" text-sm"><?php echo $email ?></a>
            </li>
            <li class="dir-ltr">
                <a href="" class=" text-sm"><?php echo $telegram ?></a>
            </li>
            <li class="dir-ltr">
                <a href="" class=" text-sm"><?php echo $phone ?></a>
            </li>
        </ul>
    </div>
</div>