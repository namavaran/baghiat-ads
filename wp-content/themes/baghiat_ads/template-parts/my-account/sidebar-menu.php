<?php
$current_user_id = get_current_user_id();

global $wpdb;
$tablename = $wpdb->prefix . 'aw_users';
$events = $wpdb->get_results("SELECT event_id FROM $tablename WHERE user_id = $current_user_id");
isset($_GET['event_id']) ? $event_id = $_GET['event_id'] : $event_id = '';


// echo $current_user_id;
// print_r($events_name);

?>


<?php
if (!empty($events) && is_array($events)) {
    ?>
    <div class="relative bg-white border-2 border-primary border-shadow rounded-2xl overflow-hidden w-full xl:w-auto xl:min-w-max">
        <aside
            class=" flex flex-row overflow-x-scroll xl:overflow-hidden gap-4 w-full xl:w-auto xl:min-w-max xl:flex-col  py-4 px-3 sm:px-1 md:px-4 h-fit text-xs sm:text-sm">
            <h2 class="text-active hidden xl:block">لیست رویداد ها</h2>
            <?php
            foreach ($events as $event) {
                $event_name = get_the_title($event->event_id);
                $event_thumbnail_url = get_the_post_thumbnail_url($event->event_id);
                ?>
                <a href="<?php echo site_url() . '/my-account' . '?event_id=' . $event->event_id ?>"
                    class="xl:w-full w-fit gap-3 flex px-3 py-2 rounded-lg <?php echo $event_id == $event->event_id ? 'bg-primary-100 border border-primary text-nowrap' : ''; ?>">
                    <img class="sm:block hidden" width="24" height="24" src="<?php echo $event_thumbnail_url; ?>" alt="" />
                    <div class="text-active text-nowrap"><?php echo $event_name ?></div>
                </a>
                <?php
            }
            ?>
        </aside>
    </div>
    <?php
}
?>