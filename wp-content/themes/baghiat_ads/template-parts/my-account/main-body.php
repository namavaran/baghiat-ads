<?php

isset($_GET['event_id']) ? $event_id = $_GET['event_id'] : $event_id = '';

$current_user_id = get_current_user_id();
$invited_users = [];

$event_name = get_post($event_id);
$event_slug = $event_name->post_name;


if ( $event_id) {
    global $wpdb;
    $tablename = $wpdb->prefix . 'aw_users';
    $invited_users = $wpdb->get_results("SELECT full_name, register_date FROM $tablename WHERE invited_by = $current_user_id And event_id = $event_id");
    $invite_code = $wpdb->get_results("SELECT invite_link FROM $tablename WHERE user_id = $current_user_id And event_id = $event_id")[0]->invite_link;
    $start_date = get_post_meta($event_id,'baghiat_ads_event_start_date',true);
    $end_date = get_post_meta($event_id,'baghiat_ads_event_finish_date',true);
    $chanel_link = get_post_meta($event_id,'baghiat_ads_event_channel_link',true);
    $current_date = date('Y-m-d H:i:s');
}
// echo '<pre>';
// print_r($invite_code);
// var_dump($end_date);
// var_dump($chanel_link);

?>

<main class="w-full">
    <section class="bg-white border-2 border-primary border-shadow rounded-2xl py-8 px-4 md:px-8 mb-6 w-full flex flex-col gap-3 ">
        <?php
        
        if ( !$event_id ) {
        ?>
            <p>
                برای مشاهده اطلاعات ثبت نام در رویداد روی رویداد مورد نظر کلیک کنید.
            </p>
        <?php
        }else{
            if($current_date < $end_date){
        ?>
            <div class="bg-white  rounded-2xl mb-6 sm:w-[70%]">
                <h2 class="font-bold text-primary">
                    لینک اختصاصی دعوت به قرعه کشی
                </h2>
                <div
                    class="mt-5 mb-3 bg-primary-100 border border-primary p-3 text-active rounded-md flex justify-between items-center gap-4">
                    <button id="copyButton" class="text-sm text-nowrap copy-button flex items-center"
                        onclick="copyText()">
                        <span>کپی کردن کد</span>
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-copy" width="28"
                            height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="rgb(59 109 102)" fill="none"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path
                                d="M7 7m0 2.667a2.667 2.667 0 0 1 2.667 -2.667h8.666a2.667 2.667 0 0 1 2.667 2.667v8.666a2.667 2.667 0 0 1 -2.667 2.667h-8.666a2.667 2.667 0 0 1 -2.667 -2.667z" />
                            <path
                                d="M4.012 16.737a2.005 2.005 0 0 1 -1.012 -1.737v-10c0 -1.1 .9 -2 2 -2h10c.75 0 1.158 .385 1.5 1" />
                        </svg>
                    </button>
                    <input dir="ltr" readonly id="baghiat_invitation_code" class="w-full bg-transparent dir outline-none"
                        value="<?php echo site_url() . '/event' . '/' . $event_slug . '?invite_code=' . $invite_code ?>">
                </div>
            </div>


        <?php
            }else{
                ?>
                    <div>
                        این رویداد منقضی شده است
                    </div>
                <?php
            }

            if ( $invited_users ) {
                ?>
                    <h2 class="font-bold text-primary">
                        کاربران دعوت شده
                    <span class="text-secondary">
                        <?php echo count($invited_users) ?>
                        نفر
                    </span>
                    </h2>
                    
                    <div class="flex flex-row flex-wrap gap-3">
                <?php
                    foreach ($invited_users as $invited_user) {
                        $full_name = $invited_user->full_name;
                        $register_date = $invited_user->register_date;
                        ?>
                        <span class="rounded-lg text-sm border  text-yellow-900  bg-gradient-to-r from-yellow-500 to-yellow-300 px-5 py-2">
                            <?php echo $full_name ?>
                            <!-- -
                            <?php echo $register_date ?> -->
                        </span>
                        <?php
                    }  
                ?>
                    </div>
                <?php
            } else {
                ?>
                <p>
                    هنوز کاربری توسط شما به رویداد دعوت نشده است
                </p>
                
                <?php
            }
        }
        ?>
        <a href="<?php echo $chanel_link ?>" target="_blank"
            class="mt-3 block text-center w-full py-3 px-4  bg-primary-gradient text-white font-vazitmatn rounded-xl">
            عضویت در کانال ایتا جهت مشاهده ی نتایج قرعه کشی
        </a>
    </section>
</main>