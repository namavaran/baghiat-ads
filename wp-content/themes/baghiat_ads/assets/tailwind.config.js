/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["../**/*.php"],
  theme: {
    fontFamily: {
      'shabnam': ['Shabnam'],
      'lalezar': ['Lalezar'],
      'vazirmatn': ['Vazirmatn'],
      'iran-yekan': ['IranYekan'],
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1440px',
    },
    extend: {
      aspectRatio: {
        '4/3': '4 / 3',
      },
      backgroundImage: {
        "primary-gradient": "var(--primary-gradient)",
        "secondary-gradient": "var(--secondary-gradient)",
        "thirdary-gradient": "var(--thirdary-gradient)",
      },
      colors: {
        "primary": "#0ea5e9",
        "primary-50": "#f0f9ff",
        "primary-100": "#e0f2fe",
        "primary-200": "#bae6fd",
        "primary-300": "#7dd3fc",
        "secondary": "#f97316",
        "active": "#0c4a6e",
      },
      
    },
  },
  plugins: [],
}
