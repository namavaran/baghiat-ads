function copyText() {
    var text = document.getElementById("baghiat_invitation_code").value;
    navigator.clipboard.writeText(text).then(function () {
        document.getElementById("copyButton").innerText = "کپی شد!";
    }, function () {
        alert("خطا در کپی کردن متن");
    });
}




jQuery(document).ready(function ($) {

  // for main event page
  if($('#media-button-container')){
    
    $('#media-button-container').on('click', function (e) {
      var video = $('#media_ads').get(0);
      if (video.paused === false) {
          video.pause();
          document.getElementById('media-button').style.display = 'block';
  
      } else {
          video.play();
          document.getElementById('media-button').style.display = 'none';
      }
    });

    const video = document.querySelector("#media_ads");
    if(video){
      video.addEventListener("pause", (event) => {
        document.getElementById('media-button').style.display = 'block';
      });
    }
    


    const popup = document.getElementById("popup-link-initial");
    const closeButton = document.getElementById("close-button");
    // const mainContent = document.getElementById("main-content");
  
    if(popup && closeButton){
      closeButton.addEventListener("click", function () {
        popup.style.display = "none";
        // mainContent.style.display = "block";
      });
    }
    

  }
  
})



  // hambrger menu
  const openMobileMenu = document.querySelector("#mobile-menu-opener");
  openMobileMenu.addEventListener("click", () => {
    var x = document.getElementById("menuList");
    if (x.classList.contains("hidden")) {
      x.classList.remove("hidden");
    } else {
      x.classList.add("hidden");
    }
  });
  