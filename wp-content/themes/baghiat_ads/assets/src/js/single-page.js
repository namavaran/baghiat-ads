document.addEventListener("DOMContentLoaded", function () {
  const popup = document.getElementById("popup-link-initial");
  const closeButton = document.getElementById("close-button");
  const mainContent = document.getElementById("main-content");

  closeButton.addEventListener("click", function () {
    popup.style.display = "none";
    mainContent.style.display = "block";
  });

  const regiserHref =
    "<?php echo get_site_url() ?>/registeration?event_id=<?php echo $post_id ?>&hash=<?php echo $hash ?><?php echo $invite_code ? '&invite_code=' . $invite_code : '' ?>";
  const video = document.getElementById("media_ads");
  const buttonContainer = document.getElementById("button-container");
  const timerContainer = document.getElementById("timer-container");
  let totalTime = 0;
  let tempTotalTime = 0;
  let startTime = 0;
  let linkExist = false;

  if (video) {
    video.addEventListener("pause", function () {
      totalTime += tempTotalTime;
    });

    video.addEventListener("play", function () {
      startTime = video.currentTime;
    });

    video.addEventListener("timeupdate", function () {
      if (video.paused) {
        return;
      }
      const currentTime = video.currentTime;
      tempTotalTime = currentTime - startTime;
      const playbackRate = ((totalTime + tempTotalTime) / video.duration) * 100;

      if (playbackRate >= 2 && linkExist === false) {
        linkExist = true;
        const link = document.createElement("a");
        link.href = regiserHref;
        link.textContent = "Click me!";
        buttonContainer.appendChild(link);
      }
      timerContainer.innerHTML = `Total watched: ${parseInt(playbackRate)}%`;
      console.log(`Total watched: ${parseInt(playbackRate)}`);
    });

    document.addEventListener("visibilitychange", () => {
      if (document.visibilityState === "visible") {
        // Tab is active, resume video playback
        // video.play();
      } else {
        // Tab is inactive, pause video
        video.pause();
      }
    });
  }
});
