jQuery(document).ready(function($) {

    const time = 60;
    let code = null;
    let messageContainer = document.querySelector('#message');
    let timerInterval = null;


    function check_phone(number){
        var regex = new RegExp("^(\\+98|0)?9\\d{9}$");
        var result = regex.test(number);
        return result;
      };

    $('#submit-btn').on('click', function(e) {
        e.preventDefault();
        e.target.disabled = true;
        messageContainer.innerHTML = "";
        removeErrors();
        let fullName = $('input[name="full_name"]').val();
        let phoneNumber = $('input[name="phone_number"]').val();
        let codeInput = $('input[name="code"]').val();
        let query = window.location.search;
        const urlParams = new URLSearchParams(query);
        let eventID = urlParams.get('event_id');
        let inviteCode = urlParams.get('invite_code');
        let security = $('input[name="security"]').val(); // Get the nonce

        let phoneNumberInput = document.querySelector('input[name="phone_number"]');
        let res = check_phone(phoneNumber)
        if(!res){
          phoneNumberInput.style.borderColor = "#DC3545";
          const newSpan = document.createElement("span");
          newSpan.innerHTML = 'شماره تلفن نامعتبر';
          newSpan.classList.add("createdElement");
          newSpan.style.color = "#DC3545";
          phoneNumberInput.after(newSpan);
          valid = false;
          e.target.disabled = false;

        }else if(fullName && code == codeInput ){

            $('.error-message').text('');
            $('input[name="phone_number"]').css("border-color", "");

            $.ajax({
                type: 'POST',
                url: ajax_object.ajax_url,
                data: {
                    action: 'register_user',
                    security: security,
                    full_name: fullName,
                    phone_number: phoneNumber,
                    event_id: eventID,
                    invite_code: inviteCode
                },
                success: function(response) {
                    if (response.success) {
                        window.location.href = response.data.redirect;
                    } else {
                        // Display error message and set border color
                        resetForm()
                        $('input[name="phone_number"]').next('.error-message').text(response.data);
                        $('input[name="phone_number"]').css("border-color", "rgb(239, 68, 68)");
                        e.target.disabled = false;
                    }
                }
            });
        }
        else if(code != codeInput){
            messageContainer.innerHTML = "کد صحیح نمی باشد"
            e.target.disabled = false;
        }
        else{
            e.target.disabled = false;
        }

    });

    function removeErrors(){
        var personalForm = document.querySelectorAll(".personal-input");
        let createdElement = document.querySelectorAll(".createdElement");
        for (let j = 0; j < personalForm.length; j++) {
            personalForm[j].style.borderColor = "#bae6fd";
        }
        createdElement.forEach(function(element) {
            element.remove();
        });
    }
    // Reset border color when phone number input changes
    $('input[name="phone_number"]').on("input", function(event) { 
        $('input[name="phone_number"]').css("border-color", "");
        $('input[name="phone_number"]').next('.error-message').text('');
    });






    

    $('#submit_phone_register').on('click', function (e) {
        e.preventDefault();
        messageContainer.innerHTML = '';
        e.target.disabled = true;
        let phoneNumber = $('input[name="phone_number"]').val();

        $.ajax({
            type: 'POST',
            url: ajax_object.ajax_url,
            data: {
                action: 'register_user_phone',
                phone_number: phoneNumber,
            },
            success: function (response) {
                if (response.success) {
                    if(response.data.result[0] > 0){
                        setTimeout(
                            resetForm, time * 1000);
    
                        display = document.querySelector('#submit_phone_register');
                        startTimer(time - 1, display);
    
                        e.target.innerHTML = 'کد ارسال شد';
                        document.getElementById('phone_number').disabled = true;
                        code = response.data.code
                    }else{
                        e.target.disabled = false;
                        console.log('error')
                    }
                    
                } else {
                    console.log(response)
                }
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function resetForm() {
        document.getElementById('submit_phone_register').disabled = false;
        document.getElementById('submit_phone_register').innerHTML = 'ارسال کد';
        document.getElementById('phone_number').disabled = false;
        document.getElementById('code').value = "";
        clearInterval(timerInterval);

    }
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        timerInterval = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.innerHTML = minutes + ":" + seconds;

            if (--timer < 0) {
                timer = duration;
                clearInterval(timerInterval);
                document.querySelector('#submit_phone_register').innerHTML = 'ارسال کد';
                code = null;
                user_id = null;
            }
        }, 1000);
    }




});
