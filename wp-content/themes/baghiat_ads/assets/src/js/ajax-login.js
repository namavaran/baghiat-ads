jQuery(document).ready(function ($) {
    const time = 60;
    let code = null;
    let user_id = null;
    let messageContainer = document.querySelector('#message');
    let timerInterval = null;

    $('#submit_phone').on('click', function (e) {
        e.preventDefault();
        messageContainer.innerHTML = '';
        e.target.disabled = true;
        let phoneNumber = $('input[name="phone"]').val();

        $.ajax({
            type: 'POST',
            url: ajax_object.ajax_url,
            data: {
                action: 'login_user',
                phone_number: phoneNumber,
            },
            success: function (response) {
                if (response.success) {
                    if(response.data.result[0] > 0){
                        setTimeout(
                            resetForm, time * 1000);
    
                        display = document.querySelector('#submit_phone');
                        startTimer(time - 1, display);
    
                        e.target.innerHTML = 'کد ارسال شد';
                        document.getElementById('phone').disabled = true;
                        // console.log('hsoh')
                        // console.log(response)
                        code = response.data.code
                        user_id = response.data.user_id
                    }else{
                        e.target.disabled = false;
                        console.log('error')
                    }
                    
                } else {
                    console.log(response)
                }
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    function resetForm() {
        document.getElementById('submit_phone').disabled = false;
        document.getElementById('submit_phone').innerHTML = 'ارسال کد';
        document.getElementById('phone').disabled = false;
        clearInterval(timerInterval);

    }
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        timerInterval = setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.innerHTML = minutes + ":" + seconds;

            if (--timer < 0) {
                timer = duration;
                clearInterval(timerInterval);
                document.querySelector('#submit_phone').innerHTML = 'ارسال کد';
                code = null;
                user_id = null;
            }
        }, 1000);
    }

    $('#submit_final').on('click', function (e) {
        e.preventDefault();
        messageContainer.innerHTML = '';
        let inputCode = $('input[name="code"]').val();
        if (inputCode == code) {

            if(user_id){
                $.ajax({
                    type: 'POST',
                    url: ajax_object.ajax_url,
                    data: {
                        action: 'login_user_finel',
                        user_id: user_id,
                    },
                    success: function (response) {
                        if (response.success) {
                            location.reload();
                            // console.log('ok')
                            // console.log(response)
                            var currentLocation = window.location;
                            window.location.href = currentLocation + "/my-account";
                        } else {
                            console.log('nok')
                        }
                    }
                });
            }else{
                messageContainer.innerHTML = 'شما هنوز در قرعه کشی ثبت نام نکرده اید';
                document.getElementById('go-back-register').style.display = 'block';
                resetForm();
            }
            
        }else{
            messageContainer.innerHTML = 'کد وارد شده صحیح نمی باشد';
        }
    });
});
