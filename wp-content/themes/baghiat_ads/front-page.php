<?php
/**
 * Front page template (home page)
 * 
 * @package Baghiat_Ads
 */

get_header();



// if (isset($_POST['submit'])) {
//     $u1 = isset($_POST['front_end']) ? 1 : 0;
//     $u2 = isset($_POST['back_end']) ? 1 : 0;
//     $u3 = isset($_POST['wordpress']) ? 1 : 0;
//     $u4 = isset($_POST['devops']) ? 1 : 0;
//     $u5 = isset($_POST['android']) ? 1 : 0;
//     $u6 = isset($_POST['ui_ux']) ? 1 : 0;


//     $full_name = $_POST['full_name'];
//     $phone_number = $_POST['phone_number'];
//     $age = $_POST['age'];
//     $field_study = $_POST['field_study'];
//     $gender = $_POST['gender'];

//     // echo "<pre>";
//     // print_r($_POST);


//     global $wpdb;
//     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
//     $charset_collate = $wpdb->get_charset_collate();
//     $tablename = $wpdb->prefix . 'aw_bootcamp_users';

//     $wpdb->insert($tablename, array(
//         'full_name' => $full_name,
//         'phone_number' => $phone_number,
//         'age' => $age,
//         'field_study' => $field_study,
//         'gender' => $gender,
//         'front_end' => $u1,
//         'back_end' => $u2,
//         'wordpress' => $u3,
//         'devops' => $u4,
//         'android' => $u5,
//         'ui_ux' => $u6
        
//     ), array('%s', '%s'));


// }


// function create_database2()
// {
//     global $wpdb;
//     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
//     $charset_collate = $wpdb->get_charset_collate();

//     // Creating user table
//     $sql_data = "CREATE TABLE {$wpdb->prefix}aw_bootcamp_users (
//         id INT PRIMARY KEY AUTO_INCREMENT,
//         full_name VARCHAR(255),
//         phone_number VARCHAR(11),
//         age INT,
//         gender VARCHAR(255),
//         field_study VARCHAR(255),
//         front_end BOOLEAN,
//         back_end BOOLEAN,
//         wordpress BOOLEAN,
//         devops BOOLEAN,
//         android BOOLEAN,
//         ui_ux BOOLEAN
//     ) $charset_collate;";
//     dbDelta($sql_data);
// }

// create_database2();
?>






<!-- 
<section class="container mx-auto">
    <form method="post" id="DDD">
        
    
    <div class="grid grid-cols-1 md:grid-cols-2 gap-x-4">
        <div class="">
            <span class="block mr-2 mb-[0.625rem] cursor-default">
            نام و نام خانوادگی
            </span>
            <input name="full_name" type="text" placeholder="" class="req border-2 border-border rounded-xl px-4 py-[0.875rem] w-full shadow-sm focus:outline-none focus:border-secondary" />
        </div>
        <div class="">
            <span class="block mr-2 mb-[0.625rem] cursor-default">
            شماره موبایل
            </span>
            <input name="phone_number" type="text" placeholder="" class="req border-2 border-border rounded-xl px-4 py-[0.875rem] w-full shadow-sm focus:outline-none focus:border-secondary" />
        </div>
        <div class="">
            <span class="block mr-2 mb-[0.625rem] cursor-default">
            سن خود را وارد کنید
            </span>
            <input name="age" type="text" placeholder="" class="req border-2 border-border rounded-xl px-4 py-[0.875rem] w-full shadow-sm focus:outline-none focus:border-secondary" />
        </div>
    


    <div>
    <span class="block mr-2 mb-[0.625rem] cursor-default">
    رشته 
    </span>
    <div class="relative rounded-lg">

        <div id="variation-dropdown-btn"
            class="flex justify-between items-center px-4 py-[0.875rem] border-2 border-border rounded-xl cursor-pointer font-medium bg-white btn-select">
            <div class="flex gap-2 items-center">
                <span class="text-text-gray" id="btn-select-view">
                    رشته خود را انتخاب کنید
                </span>
            </div>
        </div>
        <input type="text" hidden id="field_study" name="field_study" class="req">


        <div id="dropdown-menu"
            class="dropBox absolute top-full right-0 bg-white shadow-md border-2 border-border w-full z-10 p-1 rounded-lg hidden mt-1">
            <ul class="flex flex-col *:hover:cursor-pointer max-h-[200px] overflow-y-scroll">
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی کامپیوتر
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    علوم کامپیوتر
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی برق
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی معماری
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی عمران
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی مکانیک
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی شیمی
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    مهندسی صنایع
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    علوم پایه (ریاضیات، فیزیک، شیمی و...) 
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    رشته های هنری و گرافیکی
                </li>

                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    رشته های تجربی
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    دانش آموز
                </li>
                <li 
                    class="dropdown-item w-full flex gap-2 text-active p-3 hover:bg-countrySelect rounded-md">
                    سایر 
                </li>

            </ul>
        </div>
    </div>
    </div>
    </div>

    <div class="my-4 flex gap-8" id="gender-container">
        <label class="block cursor-pointer">
            <span>مرد</span>
            <input type="radio" name="gender" value="male">
        </label>

        <label class="block cursor-pointer">
            <span>زن</span>
            <input type="radio" name="gender" value="female">
        </label>
    </div>

    <div>
    <span class="block mr-2 mb-[0.625rem] cursor-default">
        به کدام یک از فیلد های زیر علاقه مند هستید؟
    </span>

    <div class="flex gap-4 flex-row justify-start flex-wrap">
        <label class="rounded-md flex flex-col justify-center gap-3 items-center cursor-pointer">
            <div class="checkbox-select !border-2 p-2 block border-slate-100 bg-white">
                <input type="checkbox" hidden class="peer" name="front_end"> 
                <img class="w-[100px] h-[100px] md:w-[200px] md:h-[200px]" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpuYdLEzBvwemix8pwsncUkLLOQqnByncadg&s" alt="">
            </div>
            <span>FrontEnd</span>
        </label>

        <label class="rounded-md flex flex-col justify-center gap-3 items-center cursor-pointer">
            <div class="checkbox-select !border-2 p-2 block border-slate-100 bg-white">
                <input type="checkbox" hidden class="peer" name="back_end">     
                <img class="w-[100px] h-[100px] md:w-[200px] md:h-[200px]" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpuYdLEzBvwemix8pwsncUkLLOQqnByncadg&s" alt="">
            </div>
            <span>BackEnd</span>
        </label>

        <label class="rounded-md flex flex-col justify-center gap-3 items-center cursor-pointer">
            <div class="checkbox-select !border-2 p-2 block border-slate-100 bg-white">
                <input type="checkbox" hidden class="peer" name="wordpress"> 
                <img class="w-[100px] h-[100px] md:w-[200px] md:h-[200px]" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpuYdLEzBvwemix8pwsncUkLLOQqnByncadg&s" alt="">
            </div>
            <span>Wordpress</span>
        </label>

        <label class="rounded-md flex flex-col justify-center gap-3 items-center cursor-pointer">
            <div class="checkbox-select !border-2 p-2 block border-slate-100 bg-white">
                <input type="checkbox" hidden class="peer" name="devops"> 
                <img class="w-[100px] h-[100px] md:w-[200px] md:h-[200px]" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpuYdLEzBvwemix8pwsncUkLLOQqnByncadg&s" alt="">
            </div>
            <span>DevOps</span>
        </label>

        <label class="rounded-md flex flex-col justify-center gap-3 items-center cursor-pointer">
            <div class="checkbox-select !border-2 p-2 block border-slate-100 bg-white">
                <input type="checkbox" hidden class="peer" name="android" > 
                <img class="w-[100px] h-[100px] md:w-[200px] md:h-[200px]" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpuYdLEzBvwemix8pwsncUkLLOQqnByncadg&s" alt="">
            </div>
            <span>Android</span>
        </label>
        <label class="rounded-md flex flex-col justify-center gap-3 items-center cursor-pointer">
            <div class="checkbox-select !border-2 p-2 block border-slate-100 bg-white">
                <input type="checkbox" hidden class="peer" name="ui_ux" > 
                <img class="w-[100px] h-[100px] md:w-[200px] md:h-[200px]" 
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpuYdLEzBvwemix8pwsncUkLLOQqnByncadg&s" alt="">
            </div>
            <span>UI UX</span>
        </label>
    </div>
    </div>


    <button type="button" name="submit" id="form-reg-btn-check" class="px-8 py-2 rounded-md bg-orange-500 text-white">ارسال</button>
    <button type="submit" hidden name="submit" id="form-reg-btn"></button>
    </form>


</section> -->





<?php
get_footer();