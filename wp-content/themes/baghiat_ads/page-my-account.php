<?php
/**
 * My Account Page
 * 
 * 
 * @package Baghiat_Ads
 */
if(!is_user_logged_in()){
    wp_redirect(site_url());
}

get_header();

?>

<div class="mt-2 container mx-auto cursor-default">
    <div class="flex flex-col gap-7 xl:flex-row">

        <!-- sidebar menu -->
        <?php get_template_part('template-parts/my-account/sidebar-menu') ?>
        
        <!-- main body -->
        <?php get_template_part('template-parts/my-account/main-body') ?>

    </div>
    
</div>


<?php

get_footer();