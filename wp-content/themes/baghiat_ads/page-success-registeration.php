<?php
/**
 * Success Message Page
 * 
 * @package Baghiat_Ads
 */

get_header();

$invite_code = null;
if (isset($_GET['invite_code'])) {
    $invite_code = $_GET['invite_code'];
    $event_id = $_GET['event_id'];
    $event_name = get_post($event_id);
    $event_slug = $event_name->post_name;
}


?>

<main class="Main container mx-auto cursor-default my-4 h-screen">

    <section
        class="personal-main-box flex flex-col justify-center items-center bg-white border-2 px-4 border-primary rounded-3xl border-shadow">
        <div class="flex flex-col items-center rounded-3xl  pt-8 my-6">
            <img src="<?php echo BAGHIAT_ADS_ICON_URI ?>/ok-register.svg" alt="">

            <div class="ok-card-footer pt-6 pb-4">
                <p
                    class="w-full py-3 px-10 mb-2 border border-primary rounded-xl bg-primary-100 font-vazitmatn text-active text-center">
                    ثبت نام شما در قرعه کشی با موفقیت انجام شد</p>
                <a href="<?php echo site_url() ?>/my-account"
                    class="block text-center w-full py-3 px-4  bg-gradient-to-l from-primary to-primary-300 border-primary border-1 text-white font-vazitmatn rounded-xl">رفتن
                    به پروفایل کاربری</a>
                <a href="https://eitaa.com/joinchat/2230846046C371c2272c6" target="_blank"
                    class="mt-3 block text-center w-full py-3 px-4  bg-secondary-gradient text-white font-vazitmatn rounded-xl tap-animation">عضویت
                    در کانال ایتا جهت مشاهده ی نتایج قرعه کشی</a>
            </div>
        </div>
        <div class="bg-white border-2 border-primary rounded-2xl py-5 px-8 mb-6 sm:w-[70%]">
            <h2 class="font-bold text-active">
                لینک اختصاصی دعوت به قرعه کشی
            </h2>
            <div
                class="mt-5 mb-3 bg-primary-100 border border-border p-3 text-active rounded-md flex justify-between items-center gap-4">
                <button id="copyButton" class="text-sm text-nowrap copy-button flex items-center"
                    onclick="copyText()">
                    <span>کپی کردن کد</span>
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-copy" width="28"
                        height="28" viewBox="0 0 24 24" stroke-width="1.5" stroke="rgb(59 109 102)" fill="none"
                        stroke-linecap="round" stroke-linejoin="round">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                        <path
                            d="M7 7m0 2.667a2.667 2.667 0 0 1 2.667 -2.667h8.666a2.667 2.667 0 0 1 2.667 2.667v8.666a2.667 2.667 0 0 1 -2.667 2.667h-8.666a2.667 2.667 0 0 1 -2.667 -2.667z" />
                        <path
                            d="M4.012 16.737a2.005 2.005 0 0 1 -1.012 -1.737v-10c0 -1.1 .9 -2 2 -2h10c.75 0 1.158 .385 1.5 1" />
                    </svg>
                </button>
                <input dir="ltr" readonly id="baghiat_invitation_code" class="w-full bg-transparent dir"
                    value="<?php echo site_url() . '/event' . '/' . $event_slug . '?invite_code=' . $invite_code ?>">
            </div>
        </div>
    </section>
</main>



<?php

get_footer();