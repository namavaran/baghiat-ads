<?php

/**
 * Single post template file. (single post page)
 * 
 * 
 * @package Baghiat_Ads
 */

get_header();

$post_id = get_the_ID();
$media_url = get_field('aw_event_video', $post_id);
$token = '08sfdMewhPUWKKD3mxUso5fKqREQDgW9TecskH0pYOt2iQzDeOdChTTfrZXmuk8K';
$hash = hash('sha256', $post_id . '-' . $token);
$invite_code = null;
if (isset($_GET['invite_code'])) {
    $invite_code = $_GET['invite_code'];
}
$extention = pathinfo($media_url, PATHINFO_EXTENSION );
$page_slug = get_post_field('post_name', $post_id);
?>

<div class="fixed left-0 top-0 w-screen h-screen flex justify-center items-center bg-black/20 backdrop-blur-sm z-30"
    id="popup-link-initial">
    <div class="bg-white rounded-2xl shadow-xl p-8">
        <button class="px-3 leading-0 py-2 bg-secondary-gradient text-white rounded-lg block mb-5 w-full">
            <a href="<?php echo site_url() ?>/login?event_slug=<?php echo $page_slug ?><?php echo $invite_code ?'&invite_code='.$invite_code :'' ?>">
                قبلاً ثبت نام کرده ام
            </a>
        </button>
        <button id="close-button" class="px-3 leading-0 py-2 text-white bg-thirdary-gradient rounded-lg block w-full">
                می خواهم ثبت نام کنم
        </button>
    </div>
</div>

<div class="container Main mx-auto px-2">
    <div class="w-full flex items-center flex-col bg-white rounded-2xl shadow-xl p-8 mt-4">
        <div class="w-full text-start mb-5">
            <h1 class="text-lg md:text-2xl text-secondary mb-3">مشاهده ویدیو</h1>
        </div>


        <?php 
            if($extention == 'mp4' || $extention == 'mkv') {
                ?>
                    <div class="relative">
                        <video class="w-full lg:max-w-screen-lg rounded-lg " src="<?php echo $media_url ?>" id="media_ads" ></video>
                        <div class=" top-0 left-0 absolute z-10 flex justify-center items-center w-full h-full" id="media-button-container">
                            <button id="media-button" class="p-2 rounded-full bg-orange-500 bg-secondary-gradient">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-player-play" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#ffffff" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <path d="M7 4v16l13 -8z" />
                            </svg>
                            </button>
                        </div>
                    </div>
                <?php
            }else if($extention == 'mp3' || $extention == 'wav') {
                ?>
                    <audio src="<?php echo $media_url ?>" controls id="media_ads"></audio>
                <?php
            }
        ?>
        <div class="w-full flex flex-col mt-6">
            <div class="flex flex-row mb-4">
                <h2>نوار پیشرفت</h2>
                :&nbsp;
                <h3 id="timer-display">0%</h3>
            </div>
            <div class="w-full h-2 rounded-full border border-secondary/50 p-0 flex items-center">
                <div class="w-[0px] bg-secondary h-2 rounded-full self-end" id="timer-container"></div>
            </div>
        </div>

        <div class="w-full text-start mt-5">
            <div class="bg-primary-100 border border-primary p-4 rounded-xl">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-info-square-rounded inline" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#0284c7" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                    <path d="M12 9h.01" />
                    <path d="M11 12h1v4h1" />
                    <path d="M12 3c7.2 0 9 1.8 9 9s-1.8 9 -9 9s-9 -1.8 -9 -9s1.8 -9 9 -9z" />
                </svg>
                <p class="text-sm md:text-base text-justify font-light mt-2 inline">برای شرکت در قرعه کشی، لطفا ابتدا ویدیو بالا را کامل مشاهده کرده سپس فرم ثبت نام برای شما ظاهر خواهد شد.</p>
            </div>
        </div>
            
    </div>




    <div class="fixed left-0 top-0 w-screen h-screen flex-col justify-center items-center bg-black/20 backdrop-blur-sm z-30 hidden" id="popup-link">
        <div class="bg-white rounded-2xl shadow-xl p-8">
            <div class="flex justify-center items-center mb-6">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-gift" width="100" height="100" viewBox="0 0 24 24" stroke-width="1.5" stroke="#7bc62d" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                <path d="M3 8m0 1a1 1 0 0 1 1 -1h16a1 1 0 0 1 1 1v2a1 1 0 0 1 -1 1h-16a1 1 0 0 1 -1 -1z" />
                <path d="M12 8l0 13" />
                <path d="M19 12v7a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-7" />
                <path d="M7.5 8a2.5 2.5 0 0 1 0 -5a4.8 8 0 0 1 4.5 5a4.8 8 0 0 1 4.5 -5a2.5 2.5 0 0 1 0 5" />
            </svg>
            </div>
            <div id="button-container" class="flex justify-center"></div>
        </div>
    </div>
    
   


    <script>


        const regiserHref = "<?php echo get_site_url() ?>/registeration?event_id=<?php echo $post_id ?>&hash=<?php echo $hash ?><?php echo $invite_code ?'&invite_code='.$invite_code :'' ?>";

        const video = document.getElementById('media_ads');
        const buttonContainer = document.getElementById('button-container');
        const timerContainer = document.getElementById('timer-container');
        const timerDisplay = document.getElementById('timer-display');
        let totalTime = 0;
        let tempTotalTime = 0;
        let startTime = 0;
        let linkExist = false

        let timeLimit = 95;
        video.addEventListener('pause', function () {
            totalTime += tempTotalTime;
        });

        video.addEventListener('play', function () {
            startTime = video.currentTime;
        });

        video.addEventListener('timeupdate', function () {
            if (video.paused) {
                return;
            }
            const currentTime = video.currentTime;
            tempTotalTime = currentTime - startTime;
            const playbackRate = (totalTime + tempTotalTime) / video.duration * 100;

            if (playbackRate >= timeLimit && linkExist === false) {
                linkExist = true
                document.getElementById('popup-link').style.display = 'flex';
                const link = document.createElement('a');
                link.href = regiserHref;
                link.classList.add("link-button");
                link.textContent = 'برای ثبت نام کلیک کنید';
                buttonContainer.appendChild(link); 
            }
            timerDisplay.innerHTML = `${parseInt(playbackRate)}%`
            timerContainer.style.width = `${parseInt(playbackRate)}%`;
            // console.log(`Total watched: ${parseInt(playbackRate)}`);
        });


        document.addEventListener('visibilitychange', () => {
            if (document.visibilityState === 'visible') {
                // Tab is active, resume video playback
                // video.play();
            } else {
                // Tab is inactive, pause video
                video.pause();
            }
        });
    </script>
</div>


<?php

get_footer();
?>